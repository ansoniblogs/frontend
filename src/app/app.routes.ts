import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PantallaafaComponent } from './components/pantallaafa/pantallaafa.component';
import { FichajesComponent } from './components/fichajes/fichajes.component';
import { ListajugadoresComponent } from './components/listajugadores/listajugadores.component';


const routes: Routes = [
    { path: 'listajugadores', component: ListajugadoresComponent},
    { path: 'fichajes/:id', component: FichajesComponent},
    { path: 'pantallaafa', component: PantallaafaComponent},
    { path: 'home', component: HomeComponent },
    { path: '**', pathMatch:'full', redirectTo: 'home' }
];


export const appRouting = RouterModule.forRoot(routes);
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { appRouting } from './app.routes'
import { ClubServices } from './components/servicios/club.services';
import { PantallaafaComponent } from './components/pantallaafa/pantallaafa.component';
import { FichajesComponent } from './components/fichajes/fichajes.component';
import { ListajugadoresComponent } from './components/listajugadores/listajugadores.component';
import { FormsModule } from '@angular/forms';
import { NavComponent } from './../app/components/nav/nav.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PantallaafaComponent,
    FichajesComponent,
    ListajugadoresComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    appRouting,
    HttpModule,
    FormsModule
  ],
  providers: [ClubServices],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { ClubServices } from '../servicios/club.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listajugadores',
  templateUrl: './listajugadores.component.html',
  styleUrls: ['./listajugadores.component.css']
})
export class ListajugadoresComponent implements OnInit {
  jugadores: any;

  inputPlayer = {
    idClub: '',
    idPlayer: ''
  }

  constructor(private clubservices: ClubServices,  private router:Router) { 
    this.clubservices.getPlayers()
      .subscribe(datados => {
        console.log(datados);
        console.log(datados.jugadores);
        this.jugadores=datados.jugadores;
      },
        error => {
          console.log("Fallo el call de la api");
          console.log(error);
        
      });
      console.log(this.jugadores);
  }

  ngOnInit() {
  }

  fichar(id: any){
    console.log("Envio idclub e idplayer")
    this.inputPlayer.idClub = this.clubservices.idClubLogeado;
    console.log(this.inputPlayer.idClub);
    this.inputPlayer.idPlayer = id;
    console.log(this.inputPlayer.idPlayer)
    
    console.log("Envio nuevo player");
   // this.clubservices.
   this.clubservices.putPlayer(this.inputPlayer)
   .subscribe(data =>{
     console.log(data)
     this.router.navigate(['/home']);
   },
   error => {
     console.log("Fallo el call de la api");
     console.log(error);
  });
}
}



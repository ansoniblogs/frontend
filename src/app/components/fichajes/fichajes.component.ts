import { Component, OnInit } from '@angular/core';
import { ClubServices } from './../servicios/club.services';
import { Router } from '@angular/router'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-fichajes',
  templateUrl: './fichajes.component.html',
  styleUrls: ['./fichajes.component.css']
})
export class FichajesComponent implements OnInit {

  contenido: any;
  clubes: any;

  constructor(private activatedRouted:ActivatedRoute, private clubservices:ClubServices, private router:Router) { 
      this.clubservices.getClubes()
      .subscribe(datados => {
        console.log(datados);
        console.log(datados.results);
        this.clubes=datados.results;
      },
        error => {
          console.log("Fallo el call de la api");
          console.log(error);
        
      });
      console.log(this.clubes);
    
    this.activatedRouted.params.subscribe(params => {
      console.log("Parametro recibido: "+params['id']);
        this.clubservices.getJugadoresDisponiblesAPI(params['id'])
        .subscribe(data => {
          console.log("Recibo los jugadores")
          console.log(data);
          this.contenido = data;
          if(!this.clubservices.testearId(params['id'])){
            this.router.navigate(['/listajugadores']);
          }
          
        },
           error => {
             console.log("fallo el call de la API");
             console.log(error)
           });
  
           console.log(this.contenido);          
    })

    console.log(this.contenido);
    console.log("FINAL");
    console.log(this.clubes);
    console.log("FINAL");

    
  }
  
  ngOnInit() {

  }
}

import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class ClubServices {

    mostrar = false;
    idClubLogeado: any;
    clubes: any;
    jugadores: any;
    
    constructor(private http:Http){

    }

    cambiarMostrar(){
      this.mostrar = true;
    }

    testearId(id:number){
      if(this.idClubLogeado==id){
        return true;
      }else{
        return false;
      }
    }

    guardarId(id: number){
      this.idClubLogeado=id;
    }

    getJugadoresDisponiblesAPI(id: number){
        console.log("Usando getJugadoresDisponiblesAPI");
        
           let header = new Headers({'Content-Type':'application/json'});
           let URL = "http://localhost:8080/futbol/jugador/"+id;
        
            return this.http.get(URL, {headers: header})
               .map(res =>{
                 console.log(res.json());
                 console.log("entro positivo getJugadoresDisponibles")
                 this.jugadores = res.json().results;
                 return res.json();
               }, err => console.log("error: "+err.json()));
    }

    getPlayers(){
      console.log("Usando getPlayers");
      let header = new Headers({'Content-Type':'application/json'});
      let URL = "http://localhost:8080/futbol/jugadores";

      return this.http.get(URL, {headers: header})
      .map(res =>{
        console.log(res.json());
        console.log("entro positivo getJugadores")
        this.jugadores = res.json();
        console.log(this.jugadores);
        console.log(this.jugadores);
        return res.json();
      }, err => console.log("error: "+err.json()))
    }

    getClubes(){
      console.log("Usando getClubes");
      let header = new Headers({'Content-Type':'application/json'});
      let URL = "http://localhost:8080/futbol/clubes";

      return this.http.get(URL, {headers: header})
      .map(res =>{
        console.log(res.json());
        console.log("entro positivo getClubes")
        this.clubes = res.json();
        console.log(this.clubes);
        console.log(this.clubes.clubes);
        return res.json();
      }, err => console.log("error: "+err.json()))
    }

    
    postPlayer(jugador: any){
      console.log("Llamo a postPlayer");
      let header = new Headers({'Content-Type':'application/json'});
      let URL = "http://localhost:8080/futbol/jugador";
      let body = jugador;

      return this.http.post(URL, body, {headers: header})
             .map(res =>{
               console.log(res.json());
               console.log("entro positivo REST postPlayer")
               return res.json();
             }, err => console.log("error: "+err.json()));
  
    }

    putPlayer(inputPlayer: any){
      console.log("Llamo a putPlayer");
      let header = new Headers({'Content-Type':'application/json'});
      let URL = "http://localhost:8080/futbol/jugadores";
      let body = inputPlayer;

      return this.http.post(URL, body, {headers: header})
             .map(res =>{
               console.log(res.json());
               console.log("entro positivo REST putPlayer")
               return res.json();
             }, err => console.log("error: "+err.json()));
    }


    tieneClub(idRec: number){
      for(let club of this.clubes){
        if(club.id==idRec){
          return true;
        }
      } 
      return false
    }  

}
import { Component, OnInit } from '@angular/core';
import { ClubServices } from '../servicios/club.services';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

 
  clubes: any;
   mostrar :boolean  = false;


  ingresoLogin = {
    nombre: '',
    password: ''
  }

  constructor(private clubservices:ClubServices, private router:Router) {
    
  

   }

  ngOnInit() {

    
    this.clubservices.getClubes()
      .subscribe(data => {
        console.log(data);
        this.clubes=data;
        console.log(this.clubes);
        console.log(this.clubes.clubes);
        this.clubes=this.clubes.clubes;
        console.log(this.clubes);
      },
        error => {
          console.log("Fallo el call de la api");
          console.log(error);
        
      });
      console.log(this.clubes);
  }



  logear(forma: NgForm){
    this.mostrar = false;
    let logeo = false;
    console.log("Entro a logear");
    for(let clu of this.clubes){
      console.log("Entro al for");
      while(this.ingresoLogin.nombre==clu.nombre){
        if(this.ingresoLogin.password==clu.password){
          console.log("Entro por el si");
          this.clubservices.guardarId(clu.id);
          console.log("Guarde el ID");
          logeo = true;
          if(this.ingresoLogin.nombre=="AFA"){
          this.router.navigate(['/pantallaafa'])
          }else{
          this.router.navigate(['/fichajes',clu.id]);
          }
        }else{
          console.log("Entro por el no");
        }
      break;
      }

      
      }if(logeo==false){
        console.log("entro al false")
        this.mostrar=true;
    }
  
     

}
}

import { Component, OnInit } from '@angular/core';
import { ClubServices } from '../servicios/club.services';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router'

@Component({
  selector: 'app-pantallaafa',
  templateUrl: './pantallaafa.component.html',
  styleUrls: ['./pantallaafa.component.css']
})
export class PantallaafaComponent implements OnInit {

  creoPlayer = {
    nombre: '',
    edad: '',
    foto: ''
  }

  constructor(private clubservices:ClubServices, private router:Router) { }

  ngOnInit() {
  }

  guardar(forma:NgForm){
    console.log("Envio nuevo player");
   // this.clubservices.
   this.clubservices.postPlayer(this.creoPlayer)
   .subscribe(data =>{
     console.log(data)
     this.router.navigate(['/home']);
   },
   error => {
     console.log("Fallo el call de la api");
     console.log(error);
   });
  }

}

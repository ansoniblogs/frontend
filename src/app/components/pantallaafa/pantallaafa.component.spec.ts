import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PantallaafaComponent } from './pantallaafa.component';

describe('PantallaafaComponent', () => {
  let component: PantallaafaComponent;
  let fixture: ComponentFixture<PantallaafaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PantallaafaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PantallaafaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
